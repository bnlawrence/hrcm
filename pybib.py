# Simple script to produce a stacked bar chart of publications for
# high resolution climate and annotate the chart with some key messages
# Used for potential one page appendix for "child-of-archer", March 2016
# Author: BNL

from pybtex import database
import operator
import matplotlib.pyplot as plt
import numpy as np


def parse_file(filename):
    """ Parse a bibtex file and return an array of bibtex data for histograms """
    yeard = {}
    bdata = database.parse_file(filename)
    for entry in bdata.entries:
        field = bdata.entries[entry].fields
        if 'journal' in field:
            submitted = 'Submit' in field['journal']
        else:
            submitted = 0
        year = int(field['year'])
        if year not in yeard:
            yeard[year] = [0, 0]
        yeard[year][submitted] += 1
        
    # it's tricky sorting dictionaries:
    ordered = sorted(yeard.items(), key=operator.itemgetter(0))
    
    # now separate out the content
    indices = [k[0] for k in ordered]
    published = [k[1][0] for k in ordered]
    submitted = [k[1][1] for k in ordered]
    
    return indices, published, submitted


def stacked_bar(indices, published, submitted):
    """ Plot a stacked bar chart from the output of a single parse_file """
    # just used for testing really.
    width = 0.9
    indices = np.array(indices)
    p1 = plt.bar(indices, published, width, color='r')
    p2 = plt.bar(indices, submitted, width, bottom=published)
    plt.xticks(indices + width/2., [str(k) for k in indices])
    plt.yticks(np.arange(0,9,1)) 
    plt.ylabel('Papers per year ')
    plt.legend((p1[0], p2[0]), ('Published','Submitted'))
    plt.show()


class MultiStack(object):
    """ A bar graph with multiple stacked objects """
    def __init__(self):
        """Instantiate to set up """
        self.irange = None
        self.indices = None
        self.datasets = []
        
    def add_barset(self, indices, published, submitted):
        """ Add a publication dataset"""
        self.datasets.append((indices, published, submitted))
        self.fix_irange(indices)
       
    def fix_irange(self, indices):
        """ Extend the known range using indices """
        if not self.irange:
            self.irange = [indices[0], indices[-1]]
        else:
            if indices[0] < self.irange[0]:
                self.irange[0] = indices[0]
            if indices[-1] > self.irange[1]:
                self.irange[1] = indices[-1]
                
    def plot(self, title, width=0.9, fontsize=14, range_y=None, range_x=None):
        """ Actually plot the stacked bars """
        
        if self.indices is None:
            self.getindices()
        indices = self.indices
        
        psets = []
        colors = ['k', 'w', 'y', 'm', 'c', 'b', 'g', 'r']
        last = np.zeros(len(self.indices))
        
        for d in self.datasets:
            
            i, p, s = d
            # reindexing wouldn't be necessary if rebasing has happened
            # but we can't rely on that.
            rp, rs = self.reindex(i, p), self.reindex(i, s)
            color = colors.pop()
            bp = plt.bar(indices, rp, width, bottom=last, color=color)
            bs = plt.bar(indices, rs, width, bottom=last+rp, color=color, hatch='/')
            psets.append(bp)
            last = last+rs+rp
        
        xt = indices + width/2
        xt = xt[0::2]
        plt.xticks(xt, [str(int(k)) for k in xt], fontsize=fontsize)
        plt.yticks(np.arange(0, 15, 1)[0::2], fontsize=fontsize)
        plt.ylabel('Papers per year ', fontsize=fontsize)
        self.annotate(psets, title)
        
        plt.show()
        
    def getindices(self):
        """ Get some nice indices using the full irange """
        assert self.irange, 'Cannot getindices without any data'
        self.irange[1] += 1
        indices = np.arange(*self.irange)
        self.indices = indices
        
    def rebase(self, ti, tp, ts):
        """ remove existing dataset values from the values passed in, and
        then add to datasets ... basically used where one of the bibtex
        files includes all the other data and that needs to be removed
        to get "the rest" """
        
        self.fix_irange(ti)
        self.getindices()
        baselined = 0
        for d in self.datasets:
            i, p, s = d
            rp, rs = self.reindex(i, p), self.reindex(i, s)
            if baselined:
                br += rp
                bs += rs
            else:
                br = rp
                bs = rs
                baselined = 1
                
        tp, ts = self.reindex(ti, tp), self.reindex(ti, ts)
        tp -= br
        ts -= bs
        self.add_barset(self.indices, tp, ts)
        
    def reindex(self, x, d):
        """ Put data onto common indices. """
        newdata = np.zeros(len(self.indices))
        offset = self.irange[0]
        for i, j in zip(x, d):
            newdata[i-offset] = j
        return newdata
        
    def annotate(self, patches, title):
        """ Annotate the plot with key messages """

        plt.title(title)
        #plt.annotate(title, xy=(2004.5, 16.5), xycoords='data', color='k', fontsize=16)
        plt.legend([p[0] for p in patches], ('UJCC', 'UPSCALE', '(Rest)'),
                   bbox_to_anchor=(0.3, 0.95))
        
        plt.annotate('UK-Japan Climate\nCollaboration (UJCC)\nEarth Simulator\n2005-2007',
                     xy=(2004.3, 6), xycoords='data', fontsize='14', color='r')
        
        plt.annotate('UPSCALE\nPRACE\n2013',
                     xy=(2010.5, 9), xycoords='data', fontsize='14', color='g')
        
        plt.annotate('Model\nDevelopment\n& Data Analysis',
                     xy=(2005.4, 0.5), xycoords='data', fontsize=14, color='k')
            

def traditional():           
    for f in ['upscale_032016.bib', 
              'HRCM_group_publications_mar16.bib',
              'UJCC_publications_mar16.bib']:
        indices, published, submitted = parse_file(f)
        stacked_bar(indices, published, submitted) 


def fancy(title, core, upscale, ujcc):
    
    ms = MultiStack()

    for f in [ujcc, upscale]:
        i, p, s = parse_file(f)
        ms.add_barset(i, p, s)
        
    i, p, s = parse_file(core)
    ms.rebase(i, p, s)
    
    ms.plot(title)


if __name__ == "__main__":
    # 2016 data
    fancy('HR Climate Programme Publications (at 03/2016)',
          'HRCM_group_publications_mar16.bib',
          'upscale_032016.bib',
          'UJCC_publications_mar16.bib'
          )

    # 2017 data
    fancy('HR Climate Programme Publications (at 02/2017)',
          'HRCM-core-201702.bib',
          'upscale_201702.bib',
          'UJCC_publications_201702.bib'
          )
