# README #

This repo holds two scripts: 

1. orog.py: for plotting some "UK focused" plots which show model resolution.
1. pybib.py: plots publication histograms based on three input files: 
    1. A complete set of group publications, and
    1. a UJCC set, and
    1. an UPSCALE set.

Input data files are not provided for orog.py, but are included for pybib.py.