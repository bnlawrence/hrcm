# Code to produce orography resolution plots.
# Original version: Andy Heaps.
# This version: Bryan Lawrence
# Use convert -trim (png), or pdfcrop (pdf), afterwards!
# NB: Data files too big to put in Git!

import cf, cfplot as cfp, numpy as np

annotations = {
    'n48': ('300km', 'N48','(from <2000)'),
    'n96': ('130km', 'N96','(from ~2002)'),
    'n216': ('60km', 'N216','(from 2005)'),
    'n512': ('25km', 'N512','(from 2012)'),
    'n1024': ('12km','N1024','(from 2013)')
    }

for resolution in annotations:
    
    # get the data
    data = cf.read('orog_input_data/'+resolution+'.orog')[4]
    
    #open the canvas
    cfp.gopen()

    # Set the map, levels and colour scale
    # cfp.cscale('wiki-2.0', below=1, above=11)
    cfp.cscale('topo_15lev')
    cfp.levs(manual=[-5000, 1, 50, 150, 300, 450, 600, 750, 1000, 1250, 1500, 1750, 2000, 2250, 2500, 2750], extend='neither')
    cfp.mapset(lonmin=-15, lonmax=20, latmin=35, latmax=60)  

    # Make the plots
    cfp.setvars(continent_thickness=0.0,
                file='orog_output_plots/'+resolution+'.png',
                axis_label_fontsize=0)
    cfp.con(data, blockfill=1, colorbar=None)
  
    # Annotate
    for i,j,k in [(0,(-14.5,48),40), (1,(-14.5,46),34), (2,(-14.5,44),34)]:
         cfp.plotvars.plot.annotate(annotations[resolution][i],
            xycoords='data',
            xy = j,
            horizontalalignment='left',
            verticalalignment='bottom',
            color='white',
            rotation='0',
            fontweight='bold',
            fontsize=k,
            )
    cfp.gclose()





